#Configuracao
theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}
cores_estat <- c('#A11D21','#003366','#FF6600','#CC9900','#CC9966','#999966','#006606','#008091','#041835','#663333','#666666')


#Selecionando diretorio e carregando pacotes
setwd("C:/Users/dell/Desktop/Unb Related/ESTAT/Projeto Fantasma")
library(stringr)
library(tidyverse)


#Lendo Arquivos
dados <- read.csv("athlete_events.csv", h = T, sep=",",dec=".")
dadoss <- read.csv("country_definitions.csv", h = T, sep = ",", dec = ".")


#Separando ano e temporada
dados$Season <- word(dados$Games, -1)
dados$Year <- word(dados$Games, 1)


#Excluindo colunas nao utilizadas
#1 identificacao
#8 coluna de ano e temporada que foi separada em 2 diferentes
#10 e 11 sao o esporte e o evento respectivamente
dados <- dados[,-c(1,8)]

#Traduzindo temporada
dados$Season[dados$Season == "Summer"] <- "Verão"
dados$Season[dados$Season =="Winter"] <- "Inverno"


  


#Analise 1
      #Selecionando apenas atletas de verao
      item1 <- dados%>%
        select(Sex,Season,Year,Name) %>%
        filter(Season == "Verão")
      #Agrupando os atletas de verao por ano e removendo os que participaram mais de uma vez
      item1 <- item1 %>% 
        group_by(Year)  %>%
        filter(!duplicated(Name))
      
      #Preparando para construcao do grafico
      item1 <- item1 %>%
        count(Sex,Year) %>%                    #Fazendo a contagem de participantes por sexo e ano
        arrange(Year)
      item1$Year<-as.numeric(item1$Year)   #transformando em numerico para correcao do grafico
      

      #Grafico
      ggplot(item1) +
        aes(x = Year, y = n, group = Sex, colour = Sex) +
        geom_line(size = 1) +
        geom_point(size = 2) +
        labs(x = "Ano", y = "Nº de Participantes",colour = "Sexo") +
        scale_x_continuous(breaks = seq(1896,2016,8))+    
        theme_estat()
      #Salvando Grafico
      ggsave("serie_historica.pdf", width = 158, height = 93, units = "mm")

  
  #Analise 2
      #Criando Nova variavel com numero absoluto de medalhas
      dados$MedalAbs <- dados$Medal
      dados$MedalAbs[dados$MedalAbs== "Bronze"] <- 1
      dados$MedalAbs[dados$MedalAbs== "Silver"] <- 1
      dados$MedalAbs[dados$MedalAbs== "Gold"] <- 1
      dados$MedalAbs[is.na(dados$MedalAbs)] <- 0
      
      #Traduzindo medalhas
      dados$Medal[dados$Medal == "Silver"] <- "Prata"
      dados$Medal[dados$Medal == "Gold"] <- "Ouro"
      
      
      #Separando os medalhistas para observar a idade
      n_medal <- dados %>%
        select(Age,MedalAbs) %>%
        drop_na() %>%
        filter(MedalAbs == 1) %>%
        count(Age,MedalAbs)
      
      #Designando medalabs para n?mero absoluto de medalhas conquistadas por idade
      n_medal <- n_medal[,-2]
      n_medal <- rename(n_medal, medalabs = n)
   
      
      #Numero de participantes por idade
      total_participantes <- dados %>%
        select(Age) %>%
        count(Age)
      total_participantes <- rename(total_participantes, n_participantes = n)
      
      
      #Calculando a distribuicao de medalhas por participantes em cada grupo de idade
      n_medal <- merge(n_medal,total_participantes, by = "Age")
      n_medal$medalrel <- (n_medal$medalabs/n_medal$n_participantes)*100
      
      #Retirando outlier que dificulta a visualizacao dos dados
      n_medal <- n_medal %>%
        filter(Age>10)
      
      
      

     #Grafico final 
      medalhistas <- dados %>%
        select(Age, Medal) %>%
        drop_na(Age,Medal)
      
      
      
      ggplot(medalhistas) +
        aes(y = Age, x = Medal) +
        geom_boxplot(fill = c("#A11D21"), width = 0.5) +
        stat_summary(
          fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
        ) +
        labs(y = "Idade", x = "Medalha") +
        scale_y_continuous(breaks = seq(5,80,5))+
        theme_estat()
      ggsave("box_med_idade.pdf", width = 158, height = 93, units = "mm")
      
      
      #Coeficiente de correlacao
      cor(n_medal$Age,n_medal$medalabs)
      
      #Histograma de distribuicao de Idade
      idade <- dados %>%
        filter(Age<50 & Age>10)
      ggplot(idade)+
        aes(x = Age)+
        geom_histogram(aes(y = 100 * (..count..) / sum(..count..)),
                       colour = "white",
                       fill = "#A11D21",
                       binwidth = 3) +
        labs(x = "Idade", y = "Frequência (%)") +
        scale_x_continuous(breaks = seq(10,80,5))+
        theme_estat()
      ggsave("hist_idade.pdf", width = 158, height = 93, units = "mm") 
      
      
      
      #Grafico de medalhas relativas por idade
      ggplot(n_medal) +
        aes(x=Age, y=medalrel, group=1) +
        geom_line(size=1,colour="#A11D21") + geom_point(colour="#A11D21",size=2) +
        labs(x="Idade", y="Conquista de Medalha 
por participante (em %)") +
        scale_x_continuous(breaks = seq(0,80,5))+
        theme_estat()
      ggsave("n_de_medalhas_rel.pdf", width = 158, height = 93, units = "mm")
      

 






           
#Analise 3
    #O arquivo continentes foi criado a partir de um encontrado no github que relacionava sigla de pais e continente,
    # mas foi completado manualmente (pq muitos paises trocaram de sigla ao longo dos anos)
    continentes <- read.csv("continentes.csv", h = T, sep = ",")         
    continentes <- continentes[,-1]
   
    dados <- merge(dados,continentes, by = "NOC")

    item3 <- dados %>%
      drop_na(Medal) %>%
      select(Continent_Name,Medal) %>%
      count(Continent_Name,Medal)
    
    legendas <- str_squish(str_c(item3$n))
    
    ggplot(item3)+
      aes(x = fct_reorder(Continent_Name, n, .desc = T), y = n, fill = Medal, label = legendas) +
      geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
      geom_text(position = position_dodge(width = .9), vjust = -0.5, hjust = 0.5, size = 2.9)+
      labs(x="Continente", y="Nº de medalhas", fill = "Medalha")+
      theme_estat()
    ggsave("medalhas_por_continente.pdf", width = 158, height = 100, units = "mm")

    
    #Informações para complementação da análise
    dados %>%
      count(Continent_Name)
    
        
#Analise 4
      #Criando coluna de IMC
      dados$imc <- round(dados$Weight/(dados$Height/100)**2, digits = 2)
      
      #Separando altura
      item4 <- dados %>%
        select(Weight,Height,Season,Year,Age,imc) %>%
        drop_na(Weight,Height,Season,Year) 

      
      #Imc boxplots
      ggplot(item4) +
        aes(y = imc, x = Season) +
        geom_boxplot(fill = c("#A11D21"), width = 0.5) +
        stat_summary(
          fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
        ) +
        labs(y = "IMC", x = "Estação") +
        scale_y_continuous(breaks = seq(5,65,5))+
        theme_estat()
      ggsave("boxplot_imc.pdf", width = 158, height = 93, units = "mm")      
      
#Analise 5
      item5 <- dados%>%
        select(Name,Medal,Event,Year) %>%
        drop_na(Medal)
      item5$medpeso[item5$Medal=="Bronze"] <- 1
      item5$medpeso[item5$Medal=="Prata"] <- 2
      item5$medpeso[item5$Medal=="Ouro"] <- 3
      
      ranking <- item5 %>% 
        group_by(Name) %>% 
        summarise(medpeso = sum(medpeso))
      
      #Coletando dados sobre o primeiro lugar
      pri <- dados %>%
        filter(Name == "Michael Fred Phelps, II") %>%
        select(Name,Medal,Event,Year,Season,Sport,Sex,NOC)
      pri %>%
      count(Medal)
      
      
      #Coletando dados sobre o segundo lugar
      seg <- dados %>%
        filter(Name == "Ole Einar Bjrndalen") %>%
        select(Name,Medal,Event,Year,Season,Sport,Sex,NOC)
      seg %>%
        count(Medal)
      
      #Coletando dados sobre o terceiro lugar
      ter <- dados %>%
        filter(Name == "Paavo Johannes Nurmi") %>%
        select(Name,Medal,Event,Year,Season,Sport,Sex,NOC)
      ter %>%
        count(Medal)
      
      #Coletando dados sobre o quarto lugar
      quar <- dados %>%
        filter(Name == 'Jennifer Elisabeth "Jenny" Thompson (-Cumpelik)') %>%
        select(Name,Medal,Event,Year,Season,Sport,Sex,NOC)
      quar %>%
        count(Medal)
      
      
      #Coletando dados sobre o quinto lugar
      quin <- dados %>%
        filter(Name == 'Sawao Kato') %>%
        select(Name,Medal,Event,Year,Season,Sport,Sex,NOC)
      quin %>%
        count(Medal)
      
#Item 6
      #Separando os pa?ses
      item6 <- dados %>%
        drop_na(NOC) %>%
        select(NOC) %>%
        count(NOC)
      #Selecionando os 5 primeiros
      item6 <- item6 %>%
        filter(n<14)
      
      #Adicionando coluna de continente
      item6 <- merge(item6,continentes, by = "NOC")
      
      #Substituindo sigla pelo nome
      item6$NOC[item6$NOC == "NRU"] <- "Nauru"
      item6$NOC[item6$NOC == "KIR"] <- "Kiribati"
      item6$NOC[item6$NOC == "SSD"] <- "Sudão do Sul"
      item6$NOC[item6$NOC == "TLS"] <- "Timor-Leste"
      item6$NOC[item6$NOC == "TUV"] <- "Tuvalu"
      
      #Grafico
      ggplot(item6) +
        aes(x = fct_reorder(NOC, desc(n)), y = n, label = n, 
            fill = fct_reorder(Continent_Name,desc(n)))+
        geom_bar(stat = "identity", width = 0.7) +
        geom_text(
          position = position_dodge(width = .9), hjust = -1,
          size = 3
        )+
        labs(x = "País", y = "Nº de participantes",fill  = "Continente")+
        scale_y_continuous(breaks = seq(0,16,2))+
        coord_flip()+
        theme_estat()
      ggsave("menor_participacao.pdf", width = 158, height = 93, units = "mm")
      
      #Coletando informacoes para analise
      item6a <- dados %>%
        filter(NOC == "NRU"|NOC =="KIR"|NOC =="SSD"|NOC =="TLS"|NOC =="TUV")
      
      #Informacoes sobre os esportes em que participaram
      item6a %>%
        count(Sport)
      
      #Informacoes sobre o sexo
      item6a %>%
        count(Sex)
      
      #Informacoes sobre o sexo total para comparacoes
      dados %>%
        filter(Year >= 1996) %>%
        count(Sex)
      
      